#include "DAUtilities.h"
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>

QString getJsonValue(QString jsonString, QString key)
{
    QJsonDocument doc = QJsonDocument::fromJson(jsonString.toUtf8());

    if(!doc.isObject())
    {
        qDebug() << "json parsing fail" << doc;
        return "";
    }

    QJsonObject obj = doc.object();
    QString ret = obj.value(key).toString();

    return ret;
}
QString setJsonValue(QString jsonString, QString val)
{
    return setJsonValue(jsonString,"COMMAND",val);
}
QString setJsonValue(QString jsonString, QString key, QString val)
{
    QJsonDocument doc;
    doc = QJsonDocument::fromJson(jsonString.toUtf8());

    QJsonObject obj;
    if(doc.isObject())
    {
        obj = doc.object();
    }
    doc.setObject(obj);
    obj.insert(key,QJsonValue::fromVariant(val));
    doc.setObject(obj);
    return doc.toJson(QJsonDocument::Compact);

}


//for time
QString getCurrentTime(int syncTime)
{
    struct timeval time;
    struct timeval add;
    add.tv_sec = 0;
    add.tv_usec = syncTime;

    time = getTime();

    timeradd(&time,&add,&time);
    return getTime(time);
}
struct timeval addTime(struct timeval aTime, struct timeval bTime)
{
    struct timeval ret;
    struct timeval a = aTime;
    struct timeval b = bTime;
    timeradd(&a,&b,&ret);

    return ret;
}

struct timeval calcDiffTime(struct timeval startTime, struct timeval endTime, int div)
{
    struct timeval ret;
    struct timeval a = endTime;
    struct timeval b = startTime;

    timersub(&a, &b, &ret);

    if(ret.tv_sec < 0 || ret.tv_usec < 0)
    {
        qDebug() << "video_" << "timer diff error";
        qDebug() << "video_" << startTime.tv_sec;
        qDebug() << "video_" << startTime.tv_usec;
        qDebug() << "video_" << endTime.tv_sec;
        qDebug() << "video_" << endTime.tv_usec;

        ret.tv_sec = 0;
        ret.tv_usec = 1000;
    }


    if(div != 0)
    {
        ret.tv_sec /= div;
        ret.tv_usec /= div;
    }
    return  ret;
}

QString getTime(struct timeval val)
{
    QString ret;

    ret.append(QString::number(val.tv_sec));
    ret.append(" ");
    ret.append(QString::number(val.tv_usec ));
    return ret;
}
struct timeval getTime(QString val)
{
    struct timeval time;
    time.tv_sec = 0;
    time.tv_usec = 0;

    if(val == "")
    {
        struct timezone timezone;
        timezone.tz_dsttime = 0;
        timezone.tz_minuteswest = 0;

        int result = gettimeofday(&time,&timezone);
        if(result == -1)
        {
            qDebug() << "fail to read Time";
        }
        return time;
    }
    else
    {
        QStringList timeList = val.split(" ");
        time.tv_sec = timeList.at(0).toULong();
        time.tv_usec = timeList.at(1).toULong();
        return time;
    }
    return time;
}

int setTime(struct timeval val)
{
    struct timezone timezone;
    timezone.tz_dsttime = 0;
    timezone.tz_minuteswest = 0;

    return settimeofday(&val,&timezone);
}
