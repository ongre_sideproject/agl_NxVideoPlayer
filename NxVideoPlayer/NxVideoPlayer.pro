#-------------------------------------------------
#
# Project created by QtCreator 2016-10-25T11:08:06
#
#-------------------------------------------------

QT += core multimedia
# quick gui qml

TARGET = NxVideoPlayer
TEMPLATE = app

# Add Playler Tool Library

#ssPKG_CONFIG_SYSROOT_DIR=/ssd/dv/build/tmp/work/s5p4418_navi_ref-agl-linux-gnueabi/NxVideoPlayer/NEXELL-0.1/recipe-sysroot
message($${PKG_CONFIG_SYSROOT_DIR})

LIBS += -L${PKG_CONFIG_SYSROOT_DIR}/usr/lib/ -lnx_drm_allocator -lnx_video_api
LIBS += -L../libnxplayer/lib/32bit -lnxmpmanager -lnxfilterhelper -lnxfilter

# Add icu libraries
LIBS += -licuuc -licui18n


INCLUDEPATH += ${PKG_CONFIG_SYSROOT_DIR}/usr/include/ ../libnxplayer/include

SOURCES += \
    main.cpp\
    DAVideoPlayer.cpp \
    CNX_MoviePlayer.cpp \
    CNX_SubtitleParser.cpp \
    DAIpcSocket.cpp \
    DAUtilities.cpp

HEADERS  += \
    DAVideoPlayer.h \
    CNX_MoviePlayer.h \
    CNX_Util.h \
    CNX_SubtitleParser.h \
    DAIpcSocket.h \
    DAUtilities.h

target.path = /home/root
INSTALLS += target


