#include "DAVideoPlayer.h"
#include "DAUtilities.h"
// Event callback
static    CallBackSignal mediaStateCb;

// Callback eos, error
static void cbEventCallback( void */*privateDesc*/, unsigned int EventType, unsigned int /*EventData*/, unsigned int /*param*/ )
{
    mediaStateCb.statusChanged(EventType);
}

// DAVideoPlayer
DAVideoPlayer::DAVideoPlayer(QObject *parent) : QObject(parent)
    , m_pNxPlayer       (nullptr)
    , m_fileUri         ("")
    , m_totalDuration   (0)
    , m_currentDuration (0)
    , m_videoWidth      (0)
    , m_videoHeight     (0)
    , m_audioTrackNumbers (0)
    , m_videoTrackNumbers (0)
    , m_volValue        (0)
    , m_pTimer          (nullptr)
    , m_DspMode         (DSP_MODE_DEFAULT)
    , m_displayMode     (DSP_NORMAL)
    , m_trackChanged    (false)
    , m_durationChanged (false)
    , m_loadCommand     (false)
    , m_pCodec          (nullptr)
    , m_bSubThreadFlag  (false)
    //, m_subtitleSync    (0)
    //, m_pSubtitleParser (NULL)
{
    //	Connect Solt Functions
    connect(&mediaStateCb, SIGNAL(mediaStatusChanged(int)), SLOT(statusChanged(int)));


    QThread *thread = new QThread;
    m_pIpcSocket = new DAIpcSocket();
    m_pIpcSocket->initCoreIpcSocket();

    connect(thread, SIGNAL(started()), m_pIpcSocket, SLOT(run()));
    connect(m_pIpcSocket, SIGNAL(receiveMessageFromGui(QString)), this, SLOT(ExtCmdProcedure(QString)));

    this->moveToThread(thread);
    m_pIpcSocket->moveToThread(thread);
    thread->start();


    m_pNxPlayer = new CNX_MoviePlayer();
    m_DspMode = DSP_MODE_DEFAULT;


    m_pTimer = new QTimer();
    //Update duration
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(updateDuration()));

    m_pTimer->start(300);


    m_playlist << "/media/BF67-CDE8/idle.mp4";
    m_playlist << "/media/BF67-CDE8/test5.mkv";
    m_playlist << "/media/BF67-CDE8/iu.mp4";



    m_pNxPlayer->OpenHandle(cbEventCallback, nullptr);
    m_index = 0;
    m_changeTrack = false;

    m_playlistTimer = new QTimer();
    connect(m_playlistTimer, SIGNAL(timeout()), this, SLOT(playlistPlay()));
    m_playlistTimer->setInterval(5000);
   // m_playlistTimer->start();
    //m_pNxPlayer->load(m_playlist.at(m_index).toStdString().c_str(), MP_TRACK_VIDEO);


}

DAVideoPlayer::~DAVideoPlayer()
{
    if(m_pNxPlayer)
    {
        NX_MediaStatus state = m_pNxPlayer->GetState();
        if( (PlayingState == state)||(PausedState == state) )
        {
            // DH.KIM bellow method handle video handle and subtitle handle both
            stopVideo();
        }

        delete m_pNxPlayer;
        m_pNxPlayer = nullptr;
    }
}


// Video Player Control
void DAVideoPlayer::loadVideo(QString fileUri)
{
    int ret;
    QString message;
    ret = m_pNxPlayer->load(fileUri.toStdString().c_str(), MP_TRACK_VIDEO);
    if(ret < 0)
     {
        QString errorMessage;
        switch (ret)
        {
        case MP_NOT_SUPPORT_VIDEOHEIGHT:
        case MP_NOT_SUPPORT_VIDEOWIDTH: errorMessage = "This resolution is not supported."; break;
        case MP_NOT_SUPPORT_AUDIOCODEC:
        case MP_NOT_SUPPORT_VIDEOCODEC: errorMessage = "This format is not supported."; break;
        default: errorMessage = "Unknown error."; break;
        }

        message = setJsonValue(nullptr,"ERROR");
        message = setJsonValue(message,"TARGET",errorMessage.toStdString().c_str());
        m_pIpcSocket->sendMessageToGui(message);

        //stopVideo();
        return;
    }
    else
    {
        m_totalDuration = m_pNxPlayer->GetMediaDuration();
        m_audioTrackNumbers = m_pNxPlayer->GetAudioTracks();
        m_videoTrackNumbers = m_pNxPlayer->GetVideoTracks();
        m_videoWidth = m_pNxPlayer->GetVideoWidth(m_pNxPlayer->m_videoTrack);
        m_videoHeight = m_pNxPlayer->GetVideoHeight(m_pNxPlayer->m_videoTrack);

        message = setJsonValue(nullptr,"LOAD");
        message = setJsonValue(message,"TARGET", fileUri.toStdString().c_str());
        message = setJsonValue(message,"MEDIA_STATUS", "LOAD");
        message = setJsonValue(message,"MEDIA_TOTAL_TIME", QString::number(m_totalDuration));
        message = setJsonValue(message,"MEDIA_RESOLUTION", QString::number(m_videoWidth));
        message = setJsonValue(message,"MEDIA_VIDEO_TRACK", QString::number(m_videoTrackNumbers));
        message = setJsonValue(message,"MEDIA_AUDIO_TRACK", QString::number(m_audioTrackNumbers));
        m_pIpcSocket->sendMessageToGui(message);

    }


}

void DAVideoPlayer::playVideo(bool sendMessage)
{
    QString message;
    m_pNxPlayer->Play();
    message = setJsonValue(nullptr,"PLAY");
    message = setJsonValue(message,"MEDIA_STATUS", "PLAY");
    m_pIpcSocket->sendMessageToGui(message);
}

void DAVideoPlayer::stopVideo(bool closeHandle)
{
    QString message;

    m_pNxPlayer->CloseSubtitle();
    m_pNxPlayer->Stop();


    message = setJsonValue(nullptr,"STOP");
    message = setJsonValue(message,"MEDIA_STATUS", "STOP");
    m_pIpcSocket->sendMessageToGui(message);
}

void DAVideoPlayer::pauseVideo(bool sendMessage)
{
    QString message;
    m_pNxPlayer->Pause();
    message = setJsonValue(nullptr,"PAUSE");
    message = setJsonValue(message,"MEDIA_STATUS", "PAUSE");
    m_pIpcSocket->sendMessageToGui(message);

}

qint32 DAVideoPlayer::setAudioSync(qint64 sync)
{

}

void DAVideoPlayer::setDisplayRatio(qint32 displayMode)
{

}


// Subtitle Control
qint32 DAVideoPlayer::openSubtitle(QString subtitleFilePath)
{

}

void DAVideoPlayer::playSubtitle()
{
}

void DAVideoPlayer::pauseSubtitle()
{
}

void DAVideoPlayer::stopSubtitle()
{

}


// Slots
void DAVideoPlayer::ExtCmdProcedure(QString cmd)
{
    qDebug() << "[DH.KIM]" << cmd;

    QString command = getJsonValue(cmd);
    QString target = getJsonValue(cmd,"TARGET");
    if(command == "LOAD")
    {
        qDebug() << "LOAD";
        loadVideo(target);

    }else if(command == "PLAY")
    {
        qDebug() << "PLAY";
        playVideo();
    }else if(command == "PAUSE")
    {
        pauseVideo();
    }



}

void DAVideoPlayer::statusChanged(int eventType)
{
    switch (eventType)
    {
        case MP_MSG_EOS:

            qDebug() << "EOS";
            break;

        case MP_MSG_DEMUX_ERR:

            qDebug() << "MP_MSG_DEMUX_ERR";
            break;

        case MP_MSG_STREAM_READY:

            qDebug() << "MP_MSG_STREAM_READY";
            m_pNxPlayer->Play();
            break;

    default:
        qDebug() << "default:" << eventType;
        break;
    }
}

void DAVideoPlayer::updateDuration()
{
    QString durationData;

    if( (m_pNxPlayer) && (StoppedState != m_pNxPlayer->GetState()))
    {



        durationData = QString::number(m_pNxPlayer->GetMediaPosition());
    }
    else
    {
        // Send 0 duration to GUI
        durationData = "0";
    }

    if (durationData.compare(QString::number(m_currentDuration)) != 0)
    {
        m_currentDuration = durationData.toInt();

        QString message = setJsonValue(nullptr,"UPDATE_DURATION");
        message = setJsonValue(message,"TARGET",durationData);
        message = setJsonValue(message,"TIME",getTime(getTime()));
        m_pIpcSocket->sendMessageToGui(message);
    }

}

void DAVideoPlayer::playlistPlay()
{

    NX_MediaStatus status = m_pNxPlayer->GetState();
    switch (status)
    {
    case StoppedState:
        qDebug() << __FUNCTION__ << "Stop";
        qDebug() << m_playlist.at(m_index);
        m_pNxPlayer->OpenHandle(cbEventCallback, nullptr);
        qDebug() << m_pNxPlayer->load(m_playlist.at(m_index).toStdString().c_str(), MP_TRACK_VIDEO);
        m_pNxPlayer->Play();


        break;
    case PlayingState:
        qDebug() << __FUNCTION__ << "playing";

        if(m_playlist.at(m_index) == "/media/BF67-CDE8/test5.mkv" && m_changeTrack == false)
        {
            qDebug() << "test";
            m_pNxPlayer->Stop();
            m_pNxPlayer->CloseHandle();
            m_pNxPlayer->addAudioTrack(1);
            //m_pNxPlayer->Play();
            m_pNxPlayer->OpenHandle(cbEventCallback, nullptr);
            m_pNxPlayer->load(m_playlist.at(m_index).toStdString().c_str(), MP_TRACK_VIDEO);

            m_changeTrack = true;
        }else {
            m_pNxPlayer->Stop();
            m_pNxPlayer->CloseHandle();

            m_index++;


            if(m_index >= m_playlist.count())
            {
                m_index = 0;
            }
            if(m_changeTrack)
            {
                m_changeTrack = false;
            }
        }

        //
        break;
    case PausedState:
        qDebug() << __FUNCTION__ << "pause";
        break;
    case ReadyState:

        qDebug() << __FUNCTION__ << "ready";
        break;
    }




}
