#ifndef DAVIDEOPLAYER_H
#define DAVIDEOPLAYER_H
#include <signal.h>
#include <QWindow>
#include <QObject>
#include <QTextCodec>
#include <QMediaPlayer>
#include <QTime>
#include <QTimer>
#include <QDebug>
#include <QTouchEvent>
#include <QJsonDocument>
#include <QJsonObject>

#include "DAIpcSocket.h"
#include <sys/time.h>
#include <time.h>
#include <unistd.h>


#include "CNX_MoviePlayer.h"
#include "CNX_SubtitleParser.h"
#include "CNX_Util.h"
//Display Mode
#define DSP_FULL 0
#define DSP_WIDE 1
#define DSP_NORMAL 2

#define CHROMAKEY "16711935"

class CallBackSignal : public QObject
{
	Q_OBJECT

public:
	CallBackSignal() {}

public slots:
	void statusChanged(int eventType)
	{
		emit mediaStatusChanged(eventType);
	}
signals:
	void mediaStatusChanged(int newValue);
};

class DAVideoPlayer : public QObject
{
	Q_OBJECT

public:
    explicit DAVideoPlayer(QObject *parent = nullptr);
    ~DAVideoPlayer();


    // Control Video Player
    void loadVideo(QString);
    void playVideo(bool sendMessage = false);
    void stopVideo(bool closeHandle = false);
    void pauseVideo(bool sendMessage = false);


    qint32 setAudioSync(qint64);
    void setDisplayRatio (int);


    // Control Subtitle Parser
    qint32 openSubtitle(QString);
    void playSubtitle();
    void pauseSubtitle();
    void stopSubtitle();

private:
    // Communicate
    DAIpcSocket *m_pIpcSocket;

    CNX_MoviePlayer *m_pNxPlayer;

    QString     m_fileUri;
    qint64      m_totalDuration;
    qint64      m_currentDuration;

    qint32      m_videoWidth;
    qint32      m_videoHeight;

    qint32      m_audioTrackNumbers;
    qint32      m_videoTrackNumbers;

    qint64      m_subtitleSync;
    int         m_volValue;

    QTimer      *m_pTimer;
    qint32      m_DspMode;
    qint32      m_displayMode;

    bool        m_trackChanged;
    bool        m_durationChanged;
    bool        m_loadCommand;

    // Subtitle
    QTextCodec*         m_pCodec;
    bool                m_bSubThreadFlag;
    //CNX_SubtitleParser  *m_pSubtitleParser;

    QStringList m_playlist;
    QTimer      *m_playlistTimer;
    int         m_index;
    bool        m_changeTrack;


private slots:
    void ExtCmdProcedure(QString cmd);
    void statusChanged(qint32 eventType);
    void updateDuration();
    void playlistPlay();

};

#endif // DAVIDEOPLAYER_H
