#ifndef DA_UTILITIES_H
#define DA_UTILITIES_H

#include <sys/time.h>
#include <time.h>
#include <unistd.h>

#include <QObject>
#include <QString>
#include <QStringList>


QString getJsonValue(QString jsonString, QString key = "COMMAND");
QString setJsonValue(QString jsonString, QString val); //default key is "COMMAND"
QString setJsonValue(QString jsonString, QString key, QString val);


//for time

QString getCurrentTime(int syncTime = 0);

struct timeval addTime(struct timeval aTime, struct timeval bTime);
struct timeval calcDiffTime(struct timeval startTime, struct timeval endTime, int div = 1);

QString getTime(struct timeval val);
struct timeval getTime(QString val = "");
int setTime(struct timeval val);


#endif // DA_UTILITIES_H
