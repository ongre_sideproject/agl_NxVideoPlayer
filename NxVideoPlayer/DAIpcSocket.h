#ifndef DAIPCSOCKET_H
#define DAIPCSOCKET_H

#include <QSocketNotifier>
#include <QObject>
#include <QThread>
#include <QDebug>
#include <QEventLoop>
#include <QTime>

#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/un.h>

#define CORE_MODE 1
#define GUI_MODE 0

#define SOCK_CORELOCALFILE "/home/root/daVideoPlayerCoreDown"
#define SOCK_GUILOCALFILE "/home/root/daVideoPlayerGuiDown"

#define BUFFER_SIZE 1024

class DAIpcSocket : public QThread
{
    Q_OBJECT

public:
    explicit DAIpcSocket();
    ~DAIpcSocket();

    void initCoreIpcSocket();
    void initGuiIpcSocket();

    void sendMessageToCore(QString);
    void sendMessageToGui(QString);

    char* getReceiveBuffer();

signals:
    void receiveMessageFromCore();
    void receiveMessageFromGui(QString);

private:
    int m_mode;

    int m_socket;
    QSocketNotifier *m_pNotifierSocketRead;

    size_t m_sizeAddress;
    char m_bufferReceive[BUFFER_SIZE];

    struct sockaddr_un m_coreLocalAddress;
    struct sockaddr_un m_guiLocalAddress;

private slots:
    void onReceiveFromCore(int);
    void onReceiveFromGui(int);
    void run();
};

#endif // DAIPCSOCKET_H
