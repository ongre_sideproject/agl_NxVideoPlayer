#include "DAIpcSocket.h"
#include <QDebug>

DAIpcSocket::DAIpcSocket() : QThread()
{

}

DAIpcSocket::~DAIpcSocket()
{

}

void DAIpcSocket::run()
{
    QSocketNotifier *notifier = new QSocketNotifier(m_socket, QSocketNotifier::Read, this);

    if (m_mode == CORE_MODE)
    {
        connect(notifier, SIGNAL(activated(int)), this, SLOT(onReceiveFromGui(int)));
    }

    else if (m_mode == GUI_MODE)
    {
        connect(notifier, SIGNAL(activated(int)), this, SLOT(onReceiveFromCore(int)));
    }

    QThread::exec();
}

//
//  init[Parts]IpcSocket
//
void DAIpcSocket::initCoreIpcSocket()
{
    m_mode = CORE_MODE;
    if (access(SOCK_CORELOCALFILE, F_OK) <= 0)
    {
        unlink(SOCK_CORELOCALFILE);
    }

    m_socket = socket(PF_FILE, SOCK_DGRAM, 0);

    if (m_socket < 0)
    {
        exit(1);
    }

    memset(&m_coreLocalAddress, 0, sizeof(m_coreLocalAddress));
    m_coreLocalAddress.sun_family = AF_UNIX;
    strcpy(m_coreLocalAddress.sun_path, SOCK_CORELOCALFILE);

    if(bind(m_socket, (struct sockaddr*)&m_coreLocalAddress, sizeof(m_coreLocalAddress)) < 0)
    {
        qDebug("bind() error\n");
        exit(1);
    }

    memset(&m_guiLocalAddress, 0, sizeof(m_guiLocalAddress));
    m_guiLocalAddress.sun_family = AF_UNIX;
    strcpy(m_guiLocalAddress.sun_path, SOCK_GUILOCALFILE);
}

void DAIpcSocket::initGuiIpcSocket()
{
    m_mode = GUI_MODE;
    if (access(SOCK_GUILOCALFILE, F_OK) <= 0)
    {
        unlink(SOCK_GUILOCALFILE);
    }

    m_socket = socket(PF_FILE, SOCK_DGRAM, 0);

    if(m_socket < 0)
    {
        exit(1);
    }

    memset(&m_guiLocalAddress, 0, sizeof(m_guiLocalAddress));
    m_guiLocalAddress.sun_family = AF_UNIX;
    strcpy(m_guiLocalAddress.sun_path, SOCK_GUILOCALFILE);

    if(bind(m_socket, (struct sockaddr*)&m_guiLocalAddress, sizeof(m_guiLocalAddress)) < 0)
    {
        qDebug("bind() error\n");
        exit(1);
    }

    memset(&m_coreLocalAddress, 0, sizeof(m_coreLocalAddress));
    m_coreLocalAddress.sun_family = AF_UNIX;
    strcpy(m_coreLocalAddress.sun_path, SOCK_CORELOCALFILE);
}

//
//  [Do]Message
//
void DAIpcSocket::sendMessageToCore(QString message)
{
    sendto(m_socket, message.toStdString().c_str(), strlen(message.toStdString().c_str()), 0, (struct sockaddr*)&m_coreLocalAddress, sizeof(m_coreLocalAddress));
}

void DAIpcSocket::sendMessageToGui(QString message)
{
    qDebug() << "[DH.KIM] Send : " << message;
    sendto(m_socket, message.toStdString().c_str(), strlen(message.toStdString().c_str()), 0, (struct sockaddr*)&m_guiLocalAddress, sizeof(m_guiLocalAddress));
}

//
//  on[Parts]Receive
//
void DAIpcSocket::onReceiveFromCore(int /*tmp*/)
{
   int szRead;

   m_sizeAddress  = sizeof(m_coreLocalAddress);
   memset(m_bufferReceive, 0, BUFFER_SIZE);
   szRead = recvfrom(m_socket, m_bufferReceive, BUFFER_SIZE, 0 , (struct sockaddr*)&m_coreLocalAddress, &m_sizeAddress);

   m_bufferReceive[szRead]  = '\0';
   emit receiveMessageFromCore();
}

void DAIpcSocket::onReceiveFromGui(int /*tmp*/)
{
    int szRead;

    m_sizeAddress  = sizeof(m_guiLocalAddress);
    memset(m_bufferReceive, 0, BUFFER_SIZE);
    szRead = recvfrom(m_socket, m_bufferReceive, BUFFER_SIZE, 0 , (struct sockaddr*)&m_guiLocalAddress, &m_sizeAddress);

    //m_bufferReceive[szRead]  = '\0';
    QString tmp;
    tmp.append(m_bufferReceive);

    emit receiveMessageFromGui(tmp);
}

//
// getReceiveBuffer
//
char* DAIpcSocket::getReceiveBuffer()
{
    return m_bufferReceive;
}
