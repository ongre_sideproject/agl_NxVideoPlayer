#include "DAVideoPlayer.h"

#include <QString>

#include <QCoreApplication>

int main(int argc, char *argv[])
{
    QCoreApplication app(argc, argv);

    DAVideoPlayer videoPlayer;

    app.exec();
}
