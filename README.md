# How to Build

- bitbake NxVideoPlayer

# Release Note about Video Player

## 190219
- 배속 재생 기능 구현

## 190221
- 비디오 레이어 투명도 조절 기능 구현

## 190226
- 비디오 디스플레이 화면 비율 조절 기능 구현

## 190227
- 비디오 디스플레이 화면 비율 조절 기능 리팩토링
- 다음/이전 버튼을 빠르게 연속하여 눌렀을 때, core가 죽는 버그 수정

## 190312
- Refactoring #1

## 190313
- Refactoring #2

## 190315
- SDK 지나친 로그 출력 Off
- 리스트 화면 스크롤 버벅임 해결
  > DrmVideoMute(NX_MPVideoMute) API 활용

## 190318
- Seek 시, Duration Bar 튀는 버그 core 파트 수정

## 190425
- 사용하지 않는 변수 초기화 삭제

## 190426
- Seek 중 실시간 화면 렌더링 구현
